import pickle
import importlib
import sys
import os
import os.path

if not os.path.exists(".carry"):
    os.mkdir(".carry")



f = open(".carry/carryList", "wb")
f.write(pickle.dumps([]))
f.close()
f = open(".carry/carryList", "rb")

try:
    MODULE_STACK = pickle.loads(f.read())
except:
    raise Exception("carryList is corrupted")

if not isinstance(MODULE_STACK, list):
    raise Exception("carryList is corrupted")
if len(MODULE_STACK) == 0:
    MODULE_STACK.append(sys.argv[0].split('.')[0].split('/')[-1])

class NotImplementedYet():
    pass

def carry(module):
    print(module, MODULE_STACK,module in MODULE_STACK)
    if module in MODULE_STACK:
        return NotImplementedYet
    else:
        MODULE_STACK.append(module)
        f = open(".carry/carryList", "wb")
        f.write(pickle.dumps(MODULE_STACK))
        f.close()
        try:
            return importlib.import_module(module)

        except:
            #print(module, "not found", MODULE_STACK)
            raise Exception("Module {} not found".format(module))

